package solutions.keeneye.wifitcp

import android.util.Log

class DebugLogging {
    companion object {

        private val IS_LOG_ON = true
        private val spamLogs = arrayOf("MapViewActivity", "NotificationManager",
            "AlertDisplayScreenSelector", "SpeedLimit", "CobraLocationManager",
            "SavePersistentDataTask", "DashFragment", "ApiServicesThreats", "CobraApplication",
            "ThreatStoreDBOpenHelper", "ThreatboundingBox")

        fun logger(message: String) {
            if (IS_LOG_ON) {
                Log.v("DebugLogging", message)
            }
        }

        fun logger(tag: String, message: String) {
            if (spamLogs.contains(tag)) return
            if (IS_LOG_ON) {
                Log.v(tag, message)
            }
        }

        fun logger(tag: String, message: String, logType: LogType) {
            if (spamLogs.contains(tag)) return
            if (IS_LOG_ON) {
                when (logType) {
                    LogType.DEBUG -> Log.d(tag, message)
                    LogType.ERROR -> Log.e(tag, message)
                    LogType.WTF -> Log.wtf(tag, message)
                }
            }
        }
    }
}
enum class LogType {
    DEBUG, ERROR, WTF
}