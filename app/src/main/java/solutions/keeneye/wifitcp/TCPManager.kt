package solutions.keeneye.wifitcp

import android.content.Context
import android.os.Handler
import android.os.Looper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import solutions.keeneye.wifitcp.DebugLogging.Companion.logger
import java.lang.Exception

class TCPManager {

    var client = TCPClient.getInstance()

    var sessionId: Int? = null
    var mediaFolder: String? = null
    var isRetried = false

    val handler: Handler = Handler(Looper.getMainLooper())

    var galleryListCallback: GalleryListCallback? = null

    var bitmapString = ""

    companion object {
        private const val TAG = "TCPManager"

        @Volatile
        private var instance: TCPManager? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: TCPManager().also { instance = it }
            }
    }

    private val tcpListener = object : TCPClient.TCPClientListener {
        override fun onClientConnected() {
            logger(TAG, "Client connected")
            requestMessage(MessageId.START_SESSION)
        }

        override fun onMessageReceived(data: String) {
            logger(TAG, "Message received: $data")
            var response: TCPResponse? = null
            try {
                response = GsonBuilder().setLenient().create().fromJson(data, TCPResponse::class.java)
            } catch (e: Exception) {
                //logger("TCP", e.message!!, LogType.ERROR)
                e.printStackTrace()
                return
            }

            // if token is expired, try to connect one more time.
            if (!response.isSuccess
                && response.msgId != MessageId.START_SESSION.value
                && !isRetried) {
                isRetried = true
                requestMessage(MessageId.START_SESSION)

                return
            } else if (!response.isSuccess) {
                galleryListCallback?.onFailure(ErrorType.TCP.SOMETHING_WENT_WRONG)
                return
            }

            when (MessageId.valueOf(response.msgId)) {
                MessageId.START_SESSION -> {
                    isRetried = false
                    sessionId = response.param?.toInt()
                    requestMessage(MessageId.GET_INFO)
                }
                MessageId.GET_INFO -> {
                    //mediaFolder = "/tmp/SD0/Normal/MAXcam360c"
                    mediaFolder = "/tmp/SD0/Photo"
                    requestMessage(MessageId.CHANGE_DIRECTORY)
                }
                MessageId.CHANGE_DIRECTORY -> {
                    requestMessage(MessageId.GET_FILE_LIST)
                }
                MessageId.GET_FILE_LIST -> {
                    //val list = response.list
                    response.listing?.let {
                        galleryListCallback?.onSuccess(it, mediaFolder!!)
                        galleryListCallback = null
                    }

                    requestMessage(MessageId.SET_CLIENT_INFO)
                    //requestMessage(MessageId.GET_THUMB)
                }
                MessageId.SET_CLIENT_INFO -> {
                    requestMessage(MessageId.GET_THUMB)
                }
                MessageId.GET_THUMB -> {
                    //read thumbnail
                    client.closeConnection(TCPClient.TCPType.SERVER)
                }
            }
        }

        override fun onFailedToConnect() {
            logger(TAG, "TCPClient: Error", LogType.ERROR)
            galleryListCallback?.onFailure(ErrorType.TCP.SOMETHING_WENT_WRONG)
        }
    }

    private val tcpListenerServer = object : TCPClient.TCPClientListener {
        override fun onClientConnected() {
            logger(TAG, "SERVER connected server")
        }

        override fun onMessageReceived(data: String) {
            bitmapString += data
            logger(TAG, "SERVER Message received: $data")
        }

        override fun onFailedToConnect() {
            logger(TAG, "SERVER: Error", LogType.ERROR)
        }

    }

    fun connectTCP() {
        if (sessionId == null) {
            client.connectClient(tcpListener, TCPClient.TCPType.CLIENT)
            client.connectClient(tcpListenerServer, TCPClient.TCPType.SERVER)

        } else {
            val enum = if (mediaFolder == null) MessageId.GET_INFO else MessageId.GET_FILE_LIST
            requestMessage(enum)
        }
    }


    private fun requestMessage(enum: MessageId) {
        val request = getTCPRequest(enum)
        val timeout = if (request.msgId == MessageId.GET_FILE_LIST.value || request.msgId == MessageId.GET_THUMB.value)
            TCPClient.TIMEOUT_GALLERY else TCPClient.TIMEOUT

        client.timeout = timeout
        handler.postDelayed({
            val requestStr = Gson().toJson(request)
            logger(TAG, "Send write data: $requestStr")
            client.writeData(requestStr, TCPClient.TCPType.CLIENT)
        }, client.timeout.toLong())
    }

    private fun getTCPRequest(enum: MessageId): TCPRequest {
        return when (enum) {
            MessageId.START_SESSION -> TCPRequest(0, enum.value)
            MessageId.CHANGE_DIRECTORY -> TCPRequest(sessionId!!, enum.value, mediaFolder)
            MessageId.SET_CLIENT_INFO -> TCPRequest(sessionId!!, enum.value, "192.168.42.2", "TCP")
            MessageId.GET_THUMB -> TCPRequest(sessionId!!, enum.value, "screenshot.png")
            else ->  TCPRequest(sessionId!!, enum.value)
        }
    }

    fun getGalleryList(callback: GalleryListCallback) {
        galleryListCallback = callback
        connectTCP()
    }

    interface GalleryListCallback {
        fun onSuccess(list: List<Any?>, mediaFolderPath: String)
        fun onFailure(error: ErrorType.TCP)
    }
}

enum class MessageId(val value: Int) {
    START_SESSION(257),
    GET_INFO(11),
    CHANGE_DIRECTORY(1283),
    GET_FILE_LIST(1282),
    SET_CLIENT_INFO(261),
    GET_FILE(1285),
    GET_THUMB(1285); //1025

    companion object {
        fun valueOf(value: Int): MessageId? = values().find { it.value == value }
    }
}

data class TCPRequest(
    @SerializedName("token") val token: Int,
    @SerializedName("msg_id") val msgId: Int,
    @SerializedName("param") val param: String? = null,
    @SerializedName("type") val type: String? = null,
    @SerializedName("offset") val offset: Int? = null
)

data class TCPResponse(
    @SerializedName("rval") val returnValue: Int,
    @SerializedName("msg_id") val msgId: Int,
    @SerializedName("param") val param: String?,
    @SerializedName("media_folder") val mediaFolderPath: String?,
    @SerializedName("listing") val listing: ArrayList<Any?>?
) {
    val isSuccess: Boolean get() = returnValue == 0
}

enum class ErrorType {;

    enum class TCP(val resources: Int) {
        SOMETHING_WENT_WRONG(R.string.error_bluetooth_something_went_wrong);

        fun toString(context: Context): String {
            return context.resources.getString(this.resources)
        }
    }
}