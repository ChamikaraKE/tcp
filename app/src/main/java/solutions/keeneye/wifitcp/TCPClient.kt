package solutions.keeneye.wifitcp

import android.os.Environment
import solutions.keeneye.wifitcp.DebugLogging.Companion.logger
import java.io.*
import java.net.Socket
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class TCPClient {

    private var socketClient: Socket? = null
    private var outputClient: PrintWriter? = null

    private var socketServer: Socket? = null
    private var outputServer: PrintWriter? = null

    var timeout = TIMEOUT

    private var tcpClientListener: TCPClientListener? = null
    private var tcpServerListener: TCPClientListener? = null

    // file download
    var bytesRead = 0
    var current = 0
    var fos: FileOutputStream? = null
    var bos: BufferedOutputStream? = null

    private val fileSize = 136195 // hardcoded TODO add correct file size

    companion object {
        private const val SERVER_ADDRESS = "192.168.42.1"
        //private const val PORT = 7878

        const val TIMEOUT = 200
        const val TIMEOUT_GALLERY = 2000

        const val TAG = "TCPClient"

        @Volatile
        private var instance: TCPClient? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: TCPClient().also { instance = it }
            }
    }

    fun connectClient(listener: TCPClientListener, tcpType: TCPType) {
        val executorService = Executors.newSingleThreadExecutor()
        executorService.execute {
            try {
                val port = if (tcpType == TCPType.CLIENT) 7878 else 8787
                val socket = Socket(SERVER_ADDRESS, port)
                val output = PrintWriter(socket.getOutputStream(), true)
                when (tcpType) {
                    TCPType.CLIENT -> {
                        socketClient = socket
                        outputClient = output
                        this.tcpClientListener = listener
                    }
                    TCPType.SERVER -> {
                        socketServer = socket
                        outputServer = output
                        this.tcpServerListener = listener
                    }
                }
                logger(TAG, "connectClient")
                listener.onClientConnected()
                readData(executorService, tcpType, listener)
            } catch (e: IOException) {
                e.printStackTrace()
                logger(TAG, "Connection failed", LogType.ERROR)
                listener.onFailedToConnect()
            }
        }
    }

    fun writeData(dataMsg: String?, tcpType: TCPType) {
        val executorService = Executors.newSingleThreadExecutor()
        executorService.execute {
            val socket = when (tcpType) {
                TCPType.CLIENT -> socketClient
                TCPType.SERVER -> socketServer
            }
            val output = when (tcpType) {
                TCPType.CLIENT -> outputClient
                TCPType.SERVER -> outputServer
            }
            val timeOut = when (tcpType) {
                TCPType.CLIENT -> timeout
                TCPType.SERVER -> 10000
            }
            val listener = when (tcpType) {
                TCPType.CLIENT -> tcpClientListener
                TCPType.SERVER -> tcpServerListener
            }
            try {
                output?.println(dataMsg)
                socket?.soTimeout = timeOut
                readData(executorService, tcpType, listener)
            } catch (e: IOException) {
                e.printStackTrace()
                logger(TAG, "write data failed", LogType.ERROR)
                listener?.onFailedToConnect()
            }
        }
    }

    private fun readData(executorService: ExecutorService,
                         tcpType: TCPType, listener: TCPClientListener?) {
        executorService.execute {
            try {
                val socket = when (tcpType) {
                    TCPType.CLIENT -> socketClient
                    TCPType.SERVER -> socketServer
                }
                if (tcpType == TCPType.SERVER) {
                    readFileData(executorService)
                }
                val scanner = Scanner(socket!!.getInputStream())
                while (scanner.hasNextLine()) {
                    val data = scanner.nextLine()
                    listener?.onMessageReceived(data)
                }

            } catch (e: IOException) {
                e.printStackTrace()
                logger(TAG, "read data failed", LogType.ERROR)
                listener?.onFailedToConnect()
            }
        }
    }

    private fun readFileData(executorService: ExecutorService) {
        executorService.execute {
            try {
                val byteArray = ByteArray(fileSize)
                val inputStream: InputStream = socketServer!!.getInputStream()

                val downloadFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                val fileToReceive = "${downloadFolder}/file.png"

                val f = File(fileToReceive)
                if (f.exists() && !f.isDirectory) {
                    logger(TAG, "File exists")
                } else {
                    logger(TAG, "File not exists, creating a new file")
                    f.createNewFile()
                }

                fos = FileOutputStream(fileToReceive)
                bos = BufferedOutputStream(fos)
                bytesRead = inputStream.read(byteArray, 0, byteArray.size)
                current = bytesRead

                do {
                    bytesRead = inputStream.read(byteArray, current, byteArray.size - current)
                    if (bytesRead >= 0) current += bytesRead
                } while (bytesRead > -1)

                bos!!.write(byteArray, 0, current)
                bos!!.flush()
                logger(TAG, "Successfully download")
                println("TCP File " + fileToReceive
                    .toString() + " downloaded (" + current.toString() + " bytes read)")
            } finally {
                if (fos != null) fos!!.close()
                if (bos != null) bos!!.close()
            }
        }
    }

    fun closeConnection(tcpType: TCPType) {
        try {
            val socket = when (tcpType) {
                TCPType.CLIENT -> socketClient
                TCPType.SERVER -> socketServer
            }
            val listener = when (tcpType) {
                TCPType.CLIENT -> tcpClientListener
                TCPType.SERVER -> tcpServerListener
            }
            socket?.close()
            listener?.onClintDisconnected()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    interface TCPClientListener {
        fun onClientConnected()
        fun onMessageReceived(data: String)
        fun onFailedToConnect()
        fun onClintDisconnected() {}
    }

    enum class TCPType {
        CLIENT, SERVER;
    }
}